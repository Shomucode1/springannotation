package com.Shubham.repository;

import java.util.List;

import com.Shubham.model.Customer;

public interface CustomerRepository {

	List<Customer> findAll();

}
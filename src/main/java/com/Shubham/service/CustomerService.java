package com.Shubham.service;

import java.util.List;

import com.Shubham.model.Customer;

public interface CustomerService {

	List<Customer> findAll();

}